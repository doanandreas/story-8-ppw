# django
from django.test import TestCase, Client
import time

# selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class UnitTest(TestCase):
    # Apakah localhost:8000 bisa diakses?
    def test_url_halaman_status_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)


class FunctionalTest(TestCase):
    # Aktivasi browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        # self.browser = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    # Tutup browser, hapus data percobaan dari models
    # lalu tunggu, baru tutup
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()
    
    # functional test
    def test_bisa_cari_buku_dan_muncul_di_table(self):
        # willy buka link di chrome
        self.browser.get('http://localhost:8000')

        # willy cek <title> bener apa gak
        self.assertIn("toko buku", self.browser.title)
        time.sleep(1)

        # willy ke search bar dan ketik "tdd"
        search_box = self.browser.find_element_by_id('search_box')
        search_box.send_keys("tdd")

        # willy klik submit
        self.browser.find_element_by_id("search_button").click()
        time.sleep(1)

        # wah willy ngeliat buku2 yang ada "tdd" nya
        self.assertIn("TDD", self.browser.page_source)

        # oke websitenya bekerja dengan baik
        time.sleep(2)
