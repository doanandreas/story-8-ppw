const book = (books) => {
    console.log("masuk ajax")
    var contents = books.items

    $("#results").empty();

    $('#results').append(
        "<thead>" +    
            "<tr>" +
                "<th>Title</th>" +
                "<th>Book Cover</th>" +
                "<th>Details</th>" +
                "<th>Author(s)</th>" +
            "</tr>"+
        "</thead>"
    );

    for(var i = 0; i < contents.length; i++){
        // satu row untuk satu buku
        var row = document.createElement("tr");

        // title content
        var name = document.createElement("td");
        var name_anchor = document.createElement("a");
        name_anchor.href = contents[i].volumeInfo.previewLink;
        name_anchor.innerHTML = contents[i].volumeInfo.title;
        name.append(name_anchor);
        $(row).append(name);

        // thumbnail content
        var thumbnail = document.createElement("td");
        var img = document.createElement("img");
        try{
            img.src = contents[i].volumeInfo.imageLinks.thumbnail;
            $(thumbnail).append(img);
        }catch{
            $(thumbnail).append("<p>No image</p>")
        }
        
        $(row).append(thumbnail);

        
        // description conteng
        var description = document.createElement("td");
        $(description).append(contents[i].volumeInfo.description);
        $(row).append(description);

        // author content
        var author = document.createElement("td");
        try{
            if(contents[i].volumeInfo.authors.length>0){            
                for(j=0; j<contents[i].volumeInfo.authors.length; j++){
                    if(j < (contents[i].volumeInfo.authors.length-1)){
                        author.innerHTML += contents[i].volumeInfo.authors[j] + ", ";
                    }else{
                        author.innerHTML += contents[i].volumeInfo.authors[j]
                    }
                }
            }else{
                console.log("gak ada authors");
                console.log(name_anchor.innerHTML);
                author.innerHTML = "Unknown author(s)"
            }
        }catch{
            console.log("gak ada authors");
            console.log(name_anchor.innerHTML);
            author.innerHTML = "Unknown author(s)"
        
        }

        $(row).append(author);

        $('#results').append(row);
    }   
}

$(document).ready(()=>{
    $('#results')[0].innerHTML = "<h3> Loading page... </h3>";
    
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=cats',
        success: function(books){
            book(books);
        }
    })


    $("#search_button").click(()=>{
        let key = $("#search_box").val();
        $('#results')[0].innerHTML = "<h3> Loading page... </h3>";

        $.ajax({
            method: 'GET',
            url: "https://www.googleapis.com/books/v1/volumes?q=" + key,
            success: function(books){
                book(books);
            }
        });
    });
});