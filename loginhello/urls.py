from django.urls import path, include
from . import views
# from django.contrib.auth.urls

app_name = 'loginhello'

urlpatterns = [
    path('login/', views.views_login, name='login'),
    path('logout/', views.views_logout, name='logout'),
    path('hello/', views.hello, name='hello'),
]