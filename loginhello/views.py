from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def hello(request):
    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1

    context = {
        'num_visits': num_visits,
    }

    return render(request, 'loginhello/hello.html', context=context)

def views_login(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            
            return redirect('loginhello:hello')
    else:
        form = AuthenticationForm()

    return render(request, 'loginhello/login.html', { 'form': form })

def views_logout(request):
    logout(request)
    return redirect('loginhello:hello')

