# django
from django.test import TestCase, Client
from django.urls import resolve
import time

# selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# loginhello
from .views import hello

class UnitTest(TestCase):
    # Apakah localhost:8000 bisa diakses?
    def test_url_halaman_status_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    # def test_views_login_udah_bener(self):
    #     response = Client.get('/accounts/login/')
    #     html_resp = response.content.
    #     self.assertEqual(response.)

    def test_pake_view_login(self):
        handler = resolve('/accounts/hello/')
        self.assertEqual(handler.func, hello)
        
    # def test_pake_view_logout(self):
    #     handler = resolve('/accounts/logout/')
    #     self.assertEqual(handler.func, views_logout)


class FunctionalTest(TestCase):
    # Aktivasi browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        # self.browser = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    # Tutup browser, hapus data percobaan dari models
    # lalu tunggu, baru tutup
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()
    
    # functional test
    def test_bisa_cari_buku_dan_muncul_di_table(self):
        # willy buka link di chrome
        self.browser.get('http://localhost:8000/accounts/login/')

        # willy cek <title> bener apa gak
        self.assertIn("Login Page", self.browser.title)
        time.sleep(1)

        # willy isi username dan password di form
        username = self.browser.find_element_by_id('id_username')
        username.send_keys("Willy")
        time.sleep(1)
        password = self.browser.find_element_by_id('id_password')
        password.send_keys("inipassword")
        time.sleep(1)

        # willy klik submit
        self.browser.find_element_by_id("login_button").click()
        time.sleep(1)

        # wah willy ngeliat namanya ditulis di browser dan dia disapa
        self.assertIn("Hello, Willy!", self.browser.page_source)
        self.assertIn("Have a nice day.", self.browser.page_source)
        time.sleep(1)

        # willy pun logout dan login setelah melakukan tes ini
        self.browser.find_element_by_id("logout_button").click()
        time.sleep(1)
        self.browser.find_element_by_id("relogin").click()

        # oke websitenya bekerja dengan baik
        time.sleep(2)
